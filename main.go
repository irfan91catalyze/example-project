package main

import (
	"latihan/controllers"
	"latihan/models"

	"github.com/gin-gonic/gin"
)

func main() {
	// initial gin
	router := gin.Default()

	//panggil koneksi database
	models.ConnectDatabase()

	router.GET("/", func(ctx *gin.Context) {

		//response JSON
		ctx.JSON(200, gin.H{
			"message": "HELLO WORLD",
		})
	})

	//membuat route get all posts
	router.GET("/api/posts", controllers.FindPosts)
	//membuat route store post
	router.POST("/api/posts", controllers.StorePost)
	//membuat route detail post
	router.GET("/api/posts/:id", controllers.FindPostById)
	//membuat route update post
	router.PUT("/api/posts/:id", controllers.UpdatePost)
	//membuat route delete post
	router.DELETE("/api/posts/:id", controllers.DeletePost)

	// server port 3000
	router.Run(":3000")
}
